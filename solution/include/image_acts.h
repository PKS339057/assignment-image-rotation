#ifndef IMAGE_TRANSFORMER_IMAGE_ACTS_H
#define IMAGE_TRANSFORMER_IMAGE_ACTS_H
#include <image.h>

struct image get_new_image(uint32_t w,uint32_t h);

struct image rotate( struct image const source );
#endif //IMAGE_TRANSFORMER_IMAGE_ACTS_H
