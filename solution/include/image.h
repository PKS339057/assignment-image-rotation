#ifndef ASSIGNMENT_IMAGE_ROTATION_IMAGE_H
#define ASSIGNMENT_IMAGE_ROTATION_IMAGE_H
#include <malloc.h>
#include <pixel.h>
struct image {
    uint64_t width, height;
    struct pixel* data;
};
#endif //ASSIGNMENT_IMAGE_ROTATION_IMAGE_H
