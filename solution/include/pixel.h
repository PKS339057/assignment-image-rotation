#ifndef ASSIGNMENT_IMAGE_ROTATION_PIXEL_H
#define ASSIGNMENT_IMAGE_ROTATION_PIXEL_H
#include <stdint.h>
struct pixel { uint8_t b, g, r; };
#endif //ASSIGNMENT_IMAGE_ROTATION_PIXEL_H
