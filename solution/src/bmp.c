#include <statuses.h>

#define BFTYPE 0x4D42
#define BIBITCOUNT 24

struct bmp_header {
uint16_t bfType;
uint32_t  bfileSize;
uint32_t bfReserved;
uint32_t bOffBits;
uint32_t biSize;
uint32_t biWidth;
uint32_t  biHeight;
uint16_t  biPlanes;
uint16_t biBitCount;
uint32_t biCompression;
uint32_t biSizeImage;
uint32_t biXPelsPerMeter;
uint32_t biYPelsPerMeter;
uint32_t biClrUsed;
uint32_t  biClrImportant;
}__attribute__((packed));


static uint8_t get_padding(uint32_t width){
    uint8_t padding=(4-(width*3)%4)%4;
    return padding;
}

static struct bmp_header rot_bmp={0};


enum read_status from_bmp( FILE* in, struct image* img ){
    if(in==NULL){
        return READ_ERROR;
    }
    struct bmp_header bmp;
    fread(&bmp,1, 54,in);
    if (bmp.bfType != BFTYPE) {
        return READ_INVALID_SIGNATURE;
    }
    else if (bmp.biBitCount != BIBITCOUNT) {
        return READ_INVALID_BITS;
    }
    else if (bmp.bfileSize!= bmp.biSizeImage + bmp.bOffBits) {
        return READ_INVALID_HEADER;
    }
    rot_bmp=bmp;
    *img= get_new_image(bmp.biWidth,bmp.biHeight);
    for(size_t i=0;i<img->height;i++){
        fread(img->data+i*bmp.biWidth,3,bmp.biWidth,in);
        fseek(in, get_padding(bmp.biWidth),SEEK_CUR);
    }
    return READ_OK;
}


enum write_status to_bmp( FILE* out, struct image const* img ){
    if(out==NULL){
        return WRITE_ERROR;
    }
    rot_bmp.biWidth=img->width;
    rot_bmp.biHeight=img->height;
    fwrite(&rot_bmp,54,1,out);

    int8_t tmp[3]={1,2,3};
    for(size_t i=0;i<img->height;i++){
        fwrite(img->data+i*img->width,3,img->width,out);
        fwrite(tmp, get_padding(img->width),1,out);
    }
    return WRITE_OK;
}

