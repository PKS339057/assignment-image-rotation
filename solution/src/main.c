#include <statuses.h>


int main( int argc, char** argv ) {

    (void) argc; (void) argv; // supress 'unused parameters' warning

    if(argc!=3){
        fprintf(stderr,"COUNT OF ARGS NOT EQUALS 3");
        return 1;
    }

    FILE* in= fopen(argv[1],"rb");
    struct image image={0};
    if(from_bmp(in,&image)!=READ_OK){
        fprintf(stderr,"READING FAILED OR FILE WASN'T OPEN");
        return 1;
    }
    struct image image_rot= rotate(image);
    FILE* out= fopen(argv[2],"wb");
    if(to_bmp(out,&image_rot)!=WRITE_OK){
        fprintf(stderr,"WRITING WAS FAILED OR FILE WASN'T OPEN");
        return 1;
    }
    if(fclose(out)!=0|| fclose(in)!=0){
        fprintf(stderr,"ERROR");
        return 1;
    }
    free(image_rot.data);
    free(image.data);

    return 0;
}
