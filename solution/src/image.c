#include <image.h>


struct image get_new_image(uint32_t w,uint32_t h){
    return (struct image){w,h, malloc(3*h*w)};
}

struct image rotate( struct image const source ) {
    struct image rotated;
    rotated= get_new_image(source.height,source.width);
    for (size_t i = 0; i < source.width; ++i) {
        for (size_t j = 0; j < source.height; ++j) {
            rotated.data[i * rotated.width + j] =source.data[source.width * (source.height -1-j) + i];
        }
    }
    return rotated;
}
